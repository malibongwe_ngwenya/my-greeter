(function(app) {

    app.component('myGreeter', {
        template: '<div>Welcome {{$ctrl.name}}, I am the greeter</div>',
        bindings:{
            name: '@'
        }
    });

})(angular.module('myGreeter', []));